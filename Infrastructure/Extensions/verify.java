package Extensions;


import static org.junit.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.WebElement;
import org.xml.sax.SAXException;
import com.relevantcodes.extentreports.LogStatus;
import Utilities.commonOps;

public class verify extends commonOps{
  
	
 	public static void login_error (List<WebElement> elem) throws IOException, SAXException, ParserConfigurationException {
		  try {
			  //can implement ExplicityWait here 
			  assertTrue(elem.size() !=0);
		      test.log(LogStatus.PASS, "The Error Showed on the Screen");
		  }
		  catch (Exception e) {
			  test.log(LogStatus.FAIL, "Failed to shown an Error on the Screen,  See details: " + e + " See Screenshot: "+ test.addScreenCapture(takeSS()));
//			  fail("Element NOT found");
		  }
		  catch (AssertionError e) {
			  test.log(LogStatus.FAIL, "Failed to shown an Error on the Screen, See details: " + e + " See Screenshot: "+ test.addScreenCapture(takeSS()));
//			  fail("Element NOT found");
		  }
		  
 	}
 	
 	
 	public static void tooltip_info (WebElement act, String imagePath) throws IOException, SAXException, ParserConfigurationException {
		  
 		try {
			  //can implement ExplicityWait here 
 			  action.moveToElement(act).perform();
			  screen.find(imagePath);
			  test.log(LogStatus.PASS, "Image Found, the tooltip appears on the screen");
		  }
		  catch (Exception e) {
			  test.log(LogStatus.FAIL, "Failed to find image and tooltip didn't appear,  See details: " + e + " See Screenshot: "+ test.addScreenCapture(takeSS()));
//			  fail("Error with finding image");
		  }
		

	}
 	
 	public static void city_dropdown (WebElement elem) throws IOException, SAXException, ParserConfigurationException {
		  
 		if(elem.isEnabled())
 		{
			elem.click();
			test.log(LogStatus.PASS, "The City field enabled");}
			else {
				 test.log(LogStatus.FAIL, "The City field disabled," +"  "+ " See Screenshot: "+ test.addScreenCapture(takeSS()));
				 fail("The City field disabled");
			}
 	}
 	
 	
 		
 		
}

	

