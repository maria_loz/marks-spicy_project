package Utilities;

import org.openqa.selenium.support.PageFactory;

import PageObjects.login_page;
import PageObjects.main_page;
import PageObjects.register_page;



public class init_pages extends base{

	public static void init()
	{
		signin = PageFactory.initElements(driver, main_page.class);
		login = PageFactory.initElements(driver, login_page.class);
		registration = PageFactory.initElements(driver, register_page.class);
	}
}
