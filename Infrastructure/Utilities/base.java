package Utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.Screen;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import PageObjects.login_page;
import PageObjects.main_page;
import PageObjects.register_page;

public class base {
	    public static WebDriver driver;
	    public static Screen screen;
	    public static Actions action;
	    public static login_page login;
	    public static main_page  signin;
	    public static register_page  registration;
	   
	    
	    public static ExtentReports extent;
	    public static ExtentTest test;
	    
	    public static String timeStamp = new SimpleDateFormat ("yyyy-MM-dd_HH-mm-ss").format(Calendar.getInstance().getTime());

}
