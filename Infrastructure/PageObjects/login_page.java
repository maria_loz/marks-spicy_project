package PageObjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class login_page 
{
	@FindBy(how = How.ID, using = "email")
	public WebElement email_field;
	
	@FindBy(how = How.ID, using = "passwd")
	public WebElement passwd_field;
	
	@FindBy(how = How.ID, using = "SubmitLogin")
	public WebElement submit;
	
	@FindBy(how = How.CSS, using = "div[class='form-group form-error']")
	public  List<WebElement> element_error;
	
	@FindBy(how = How.ID, using = "email_create")
	public WebElement email_create;
	
	@FindBy(how = How.ID, using = "SubmitCreate")
	public WebElement SubmitCreate;
	
	



}
