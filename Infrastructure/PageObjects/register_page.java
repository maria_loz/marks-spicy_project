package PageObjects;



import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class register_page {
	@FindBy(how = How.ID, using = "email")
	public WebElement email_field;
	
	@FindBy(how = How.ID, using = "password")
	public WebElement password_field;
	
	@FindBy(how = How.ID, using = "password2")
	public WebElement confirm_password;
	
	
	@FindBy(how = How.ID, using = "CivMlle")
	public WebElement radioButton_Miss;
	
	
	@FindBy(how = How.ID, using = "nom")
	public WebElement last_name;
	
	@FindBy(how = How.ID, using = "prenom")
	public WebElement first_name;
	
	@FindBy(how = How.ID, using = "dateJour")
	public WebElement date_dates;
	
	@FindBy(how = How.ID, using = "dateMois")
	public WebElement date_month;
	
	@FindBy(how = How.ID, using = "dateAnnee")
	public WebElement date_year;
	
	
	@FindBy(how = How.ID, using = "adresse")
	public WebElement adress_field;
	
	@FindBy(how = How.ID, using = "adresseDetail")
	public WebElement adress_details;
	
	@FindBy(how = How.ID, using = "adresseDetail2")
	public WebElement adress_details2;
	
	@FindBy(how = How.ID, using = "codePostal")
	public WebElement zip_code;
	
	@FindBy(how = How.ID, using = "ville")
	public WebElement city;
	
	@FindBy(how = How.ID, using = "lieuDit")
	public WebElement state;
	
	@FindBy(how = How.ID, using = "telephonePortable")
	public WebElement mobile_number;
	

	@FindBy(how = How.ID, using = "telephoneFixe")
	public WebElement home_number;
	
	@FindBy(how = How.ID, using = "BtnCreationSubmit")
	public WebElement submit;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
