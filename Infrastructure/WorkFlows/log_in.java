package WorkFlows;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;


import org.xml.sax.SAXException;
import Extensions.click;
import Extensions.update;
import Utilities.commonOps;

public class log_in extends commonOps
{
	public static void submit (String email_value, String password_value) throws IOException, SAXException, ParserConfigurationException, InterruptedException {
		click.go(signin.signin_button);
		update.text(login.email_field, email_value);
		update.text(login.passwd_field, password_value);
//		click.go(login.submit);
	}
	
	
}
