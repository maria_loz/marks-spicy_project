package WorkFlows;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;


import org.xml.sax.SAXException;

import Extensions.click;
import Extensions.update;

import Utilities.commonOps;

public class create_account extends commonOps {
	
	public static void connection_setting (String email_create, String email, String password, String password_confirm) throws IOException, SAXException, ParserConfigurationException 
	{
		update.text(login.email_create, email_create);
		click.go(login.SubmitCreate);
		update.text(registration.email_field, email);
		update.text(registration.password_field, password);
		update.text(registration.confirm_password, password_confirm);
	}

	
	public static void personal_info (String last_name, String first_name, String date_dates, String date_month, String date_year) throws IOException, SAXException, ParserConfigurationException 
	{
		
//        click.go(registration.radioButton_Miss);
		update.text(registration.last_name, last_name);
		update.text(registration.first_name, first_name);
		update.text(registration.date_dates, date_dates);
		update.text(registration.date_month, date_month);
		update.text(registration.date_year, date_year);
	}
	
	public static void adress_info (String adress_field, String adress_details, String adress_details2, String zip_code, String state, String mobile_number, String home_number ) throws IOException, SAXException, ParserConfigurationException, InterruptedException 
	{
		

		update.text(registration.adress_field, adress_field);
		update.text(registration.adress_details, adress_details);
		update.text(registration.adress_details2, adress_details2);
		update.text(registration.zip_code, zip_code);
		update.text(registration.state, state);
		update.text(registration.mobile_number, mobile_number);
		update.text(registration.home_number, home_number);
		
		
		
	}
}


