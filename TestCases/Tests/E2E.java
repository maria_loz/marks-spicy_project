package Tests;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.sikuli.script.FindFailed;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import Extensions.click;
import Extensions.verify;
import Utilities.commonOps;
import WorkFlows.create_account;
import WorkFlows.log_in;

public class E2E extends commonOps {
	
  @Test
  //check if an error displayed if user doesn't exist
  public static void test01_signin() throws IOException, SAXException, ParserConfigurationException, InterruptedException, FindFailed 
  {
	  
	  log_in.submit("test@test.com", "ThisIsT3st");
	  verify.login_error(login.element_error);	
  }
  
  @Test
  //tooltip validation when email was written with incorrect format
  public static void test02_tooltip() throws IOException, SAXException, ParserConfigurationException, InterruptedException 
  {
	  log_in.submit("test", "");
	  verify.tooltip_info(login.email_field, "./ImageRepository/tooltip.png");
	 
  }
  
  
  @Test
  //check if an error displayed when email and password fields are blank
  public static void test03_blank_fields() throws IOException, SAXException, ParserConfigurationException, InterruptedException 
  {
	  log_in.submit("", "");
	  verify.login_error(login.element_error);
	  
	 
  }
  
  
  @Test
  //check if the city populates when enter zip_code and check if the registration is successful
  public static void test04_registration() throws IOException, SAXException, ParserConfigurationException, InterruptedException 
  {
	  create_account.connection_setting("test@test.com", "test@test.com", "test", "test");
	  create_account.personal_info("Annela", "Ann", "10", "10", "1988");
	  create_account.adress_info("Test", "Test2", "Test2", "10005", "US", "05678565", "88789878");
	  verify.city_dropdown(registration.city);
	  click.submit_form(registration.submit);
	 
  }
}
